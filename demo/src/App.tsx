import RouterView from "./router/routerView";

function App() {
  return (
    <div className="App">
       <RouterView />
    </div>
  );
}

export default App;
