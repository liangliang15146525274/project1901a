

import styles from "./styles.module.scss"
import {Outlet} from "react-router-dom"
const Main=()=>{
    return (
         <main className={styles.main}>
             <div className={styles.warp}>
                 <Outlet />
             </div>
         </main>
    )
}

export default Main;