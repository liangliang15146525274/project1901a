
import {FC} from 'react'
import styles from "./styles.module.scss"
import {NavLink} from "react-router-dom"

//interface 和type  type支持联合类型，interface不支持  type Props=string|number


//FC 函数组件对应的类型
const Herder:FC=()=>{
     //使用
    return (
         <header className={styles.head}>
              <div className={styles.warp}>
                  <div className={styles.txt}>
                     {/* <img src="" alt="" />  */}
                     <span>logger</span>
                     <ul>
                        <li><NavLink to={"/stair/article"}>文章</NavLink></li>
                        <li><NavLink to={"/stair/archives"}>归档</NavLink></li>
                        <li><NavLink to={"/stair/knowledge"}>知识小册</NavLink></li>
                        <li><NavLink to={"/stair/message"}>留言板</NavLink></li>
                        <li><NavLink to={"/stair/here"}>关于</NavLink></li>
                        
                     </ul>
                  </div>
                  <div>
                       切换
                  </div>
              </div>
         </header>
    )
}

export default Herder;