
import {applyMiddleware,legacy_createStore,combineReducers } from "redux";

import thunk from "redux-thunk";

import logger from "redux-logger";

import artcleRedcer from "./artcleRedcer"
import KnowledgeReduce from './Knowledge'
import archivesReduce from './archivesReduce'
const reducer=combineReducers({
    artcleRedcer,
    KnowledgeReduce,
    archivesReduce
})


export default legacy_createStore(reducer,applyMiddleware(thunk,logger));