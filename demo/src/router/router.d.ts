

//路由验证

export type RouterList=RouterItem[];


export interface RouterItem{
     path:string,
     redirect?:string,
     component?:any,
     name?:string,
     children?:RouterItem[]
}