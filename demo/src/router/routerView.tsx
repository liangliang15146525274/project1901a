
import routes from "./routerList";

import { RouterList,RouterItem} from "./router"

import { Suspense } from "react";
import { Route, Routes, BrowserRouter, Navigate } from "react-router-dom"
function RouterView() {
    const renderRouter = (routes: RouterList) => {
        return routes.map((item:RouterItem, index:number) => {
            return <Route 
            key={index} 
            path={item.path} 
            element={item.redirect ? <Navigate to={item.redirect || ""}></Navigate> : <item.component></item.component>}>
                  {
                    item.children && renderRouter(item.children)
                  }
            </Route>
        })
    }
    return (
        <BrowserRouter>
         <Suspense fallback="正在加载....">
               <Routes>
                    {
                        renderRouter(routes)
                    }
               </Routes>
         </Suspense>
        </BrowserRouter>
    )
}

export default RouterView;