
import { lazy } from "react";

const RouterList = [
    {
        path: "/",
        redirect: "/stair"
    },
    {
        path: "/stair",
        name: "stair",
        component: lazy(() => import("../views/stair/index")),
        children: [ 
            {
                path: "/stair/article",
                name: "article",
                component: lazy(() => import("../views/stair/article")),
            },

            {
                path: "/stair/archives",
                name: "archives",
                component: lazy(() => import("../views/stair/archives")),
            },
            {
                path: "/stair/knowledge",
                name: "knowledge",
                component: lazy(() => import("../views/stair/knowledge")),
            },
            {
                path: "/stair/message",
                name: "message",
                component: lazy(() => import("../views/stair/message")),
            },
            {
                path: "/stair/here",
                name: "here",
                component: lazy(() => import("../views/stair/here")),
            },
            {
                path: "/stair",
                redirect: "/stair/article"
            },
        ]
    }
]


export default RouterList;