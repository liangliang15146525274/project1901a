import { Dispatch } from "redux";
import request from '../utils/request'
export const getArchives = () => {
  return async (dispatch: Dispatch) => {
    let res:any = await request.get("/api/article/archives"); 
    console.log(res);
    
    if (res.status === 200) {
      dispatch({
        type: "GET_ARCHIVES",
        payload: res.data.data,
      });
    }
  };
};