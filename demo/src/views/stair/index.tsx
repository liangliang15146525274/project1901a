

import Herder from "../../components/header";
import Footer from "../../components/footer";
import Main from "../../components/main";
function Stair() {
  return (
    <div className="stair">
         <Herder />
         <Main />
         <Footer />
    </div>
  )
}


export default Stair;