import { FC, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as api from "../../../action/archives";
interface FuntionArchives {
  getArchives(): void;
}
const Archives: FC<FuntionArchives> = () => {
  const dispatch = useDispatch();
  const list = useSelector((store: any) => {
    return store.archivesReduce.list
  });
  console.log(list);

  useEffect(() => {
    dispatch(api.getArchives() as any);
  }, [dispatch]);

  return <div>
     {
      
     }
  </div>;
};

export default Archives;
